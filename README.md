About
======

This is a customized version of `tcp_probe.c` based on tcp_probe_fixed by Timo Dörr.

This version adds a module argument to control flushing the output to userspcae.

For more information and documentation on original `tcp_probe`, see:

	<http://www.linuxfoundation.org/collaborate/workgroups/networking/tcpprobe>


Install & Usage
===============

Get and install matching linux-header package for your kernel and run `make`.

Usage:
	# Load module
	make load

	# Load module with specific arguments, filter on port 6789 and flush every third packet that matches the filter
	make load PORT=6789 FLUSH=3
